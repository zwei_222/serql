﻿//----------------------------------------------------------------------
// <copyright file="SqlObjectExtension.cs" company="SerQL">
// Copyright (c) @zwei_222. All rights reserved.
// </copyright>
// <author>zwei_222</author>
// <date>2018/09/27</date>
// <summary>ISqlObject interface extension class</summary>
//----------------------------------------------------------------------

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SerQL.Core;

namespace SerQL.Extensions
{
    /// <summary>
    /// ISqlObject interface extension class
    /// </summary>
    public static class SqlObjectExtension
    {
        /// <summary>
        /// Returns ISqlObject object's deep clone.
        /// </summary>
        /// <returns>The clone.</returns>
        /// <param name="source">Source.</param>
        public static T DeepClone<T>(this T source)
            where T : ISqlObject
        {
            using (var memoryStream = new MemoryStream())
            {
                var binaryFormatter = new BinaryFormatter();

                binaryFormatter.Serialize(memoryStream, source);
                memoryStream.Seek(0, SeekOrigin.Begin);
                return (T)binaryFormatter.Deserialize(memoryStream);
            }
        }
    }
}
