﻿//----------------------------------------------------------------------
// <copyright file="ConditionBuilder.cs" company="SerQL">
// Copyright (c) @zwei_222. All rights reserved.
// </copyright>
// <author>zwei_222</author>
// <date>2018/09/27</date>
// <summary>Condition builder class</summary>
//----------------------------------------------------------------------

using System;
using SerQL.Extensions;

namespace SerQL.Core
{
    /// <summary>
    /// Condition builder class.
    /// </summary>
    public class ConditionBuilder
    {
        private MultiCondition condition;

        public ConditionBuilder(Condition condition)
        {
            this.condition = new MultiCondition(condition, true);
        }

        public ConditionBuilder(ISqlObject leftHandSide, IOperator baseOperator, ISqlObject rightHandSide)
        {
            var singleCondition = new SingleCondition(leftHandSide, baseOperator, rightHandSide);

            this.condition = new MultiCondition(singleCondition, true);
        }

        public ConditionBuilder And(Condition condition)
        {
            this.condition.Add(LogicOperator.And(), condition);
            return this;
        }

        public ConditionBuilder And(ISqlObject leftHandSide, IOperator baseOperator, ISqlObject rightHandSide)
        {
            var singleCondition = new SingleCondition(leftHandSide, baseOperator, rightHandSide);

            this.condition.Add(LogicOperator.And(), singleCondition);
            return this;
        }

        public ConditionBuilder Or(Condition condition)
        {
            this.condition.Add(LogicOperator.Or(), condition);
            return this;
        }

        public ConditionBuilder Or(ISqlObject leftHandSide, IOperator baseOperator, ISqlObject rightHandSide)
        {
            var singleCondition = new SingleCondition(leftHandSide, baseOperator, rightHandSide);

            this.condition.Add(LogicOperator.Or(), singleCondition);
            return this;
        }

        public Condition ToCondition()
        {
            return this.condition.DeepClone();
        }
    }
}
