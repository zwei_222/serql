﻿//----------------------------------------------------------------------
// <copyright file="MultiCondition.cs" company="SerQL">
// Copyright (c) @zwei_222. All rights reserved.
// </copyright>
// <author>zwei_222</author>
// <date>2018/09/27</date>
// <summary>Multi condition.</summary>
//----------------------------------------------------------------------

using System.Collections.Generic;

namespace SerQL.Core
{

    /// <summary>
    /// Multi condition.
    /// </summary>
    internal class MultiCondition : Condition
    {
        /// <summary>
        /// The conditions dictionary.
        /// </summary>
        private readonly IDictionary<Condition, IOperator> conditions;

        /// <summary>
        /// The conditions hierarchy.
        /// </summary>
        private bool isRoot;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="condition">Condition.</param>
        /// <param name="isRoot">If set to <c>true</c> is root condition.</param>
        public MultiCondition(Condition condition, bool isRoot)
        {
            this.conditions = new Dictionary<Condition, IOperator>
                {
                    { condition, null }
                };

            this.isRoot = isRoot;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:SerQL.Core.Condition.MultiCondition"/> is root.
        /// </summary>
        /// <value><c>true</c> if is root; otherwise, <c>false</c>.</value>
        public bool IsRoot
        {
            get
            {
                return this.isRoot;
            }

            set
            {
                this.isRoot = value;
            }
        }

        /// <summary>
        /// Add the specified logic operator and condition.
        /// </summary>
        /// <param name="baseOperator">Logic operator.</param>
        /// <param name="condition">Condition.</param>
        public void Add(IOperator baseOperator, Condition condition)
        {
            var multiCondition = condition as MultiCondition;

            if (condition != null)
            {
                multiCondition.IsRoot = false;
            }

            this.conditions.Add(condition, baseOperator);
        }

        /// <summary>
        /// Returns the conditions statements.
        /// </summary>
        /// <returns>The query string.</returns>
        public override string ToQueryString()
        {
            Condition lastCondition = null;

            foreach (var conditionItem in this.conditions)
            {
                var condition = conditionItem.Key;
                var baseOperator = conditionItem.Value;

                if (baseOperator == null)
                {
                    lastCondition = condition;

                    continue;
                }

                var singleCondition = new SingleCondition(lastCondition, baseOperator, condition);
                lastCondition = singleCondition;
            }

            return this.isRoot ? lastCondition.ToQueryString() : string.Format("( {0} )", lastCondition.ToQueryString());
        }
    }
}
