﻿//----------------------------------------------------------------------
// <copyright file="SingleCondition.cs" company="SerQL">
// Copyright (c) @zwei_222. All rights reserved.
// </copyright>
// <author>zwei_222</author>
// <date>2018/09/27</date>
// <summary>Single condition.</summary>
//----------------------------------------------------------------------

namespace SerQL.Core
{
    /// <summary>
    /// Single condition.
    /// </summary>
    internal class SingleCondition : Condition
    {
        /// <summary>
        /// The left hand side.
        /// </summary>
        private readonly ISqlObject leftHandSide;

        /// <summary>
        /// The right hand side.
        /// </summary>
        private readonly ISqlObject rightHandSide;

        /// <summary>
        /// The operator.
        /// </summary>
        private readonly IOperator baseOperator;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="leftHandSide">The left hand side.</param>
        /// <param name="baseOperator">The operator.</param>
        /// <param name="rightHandSide">The right hand side.</param>
        public SingleCondition(ISqlObject leftHandSide, IOperator baseOperator, ISqlObject rightHandSide)
        {
            this.leftHandSide = leftHandSide;
            this.rightHandSide = rightHandSide;
            this.baseOperator = baseOperator;
        }

        /// <summary>
        /// Returns the condition statements.
        /// </summary>
        /// <returns>The query string.</returns>
        public override string ToQueryString()
        {
            return this.baseOperator.ToQueryString(this.leftHandSide, this.rightHandSide);
        }
    }
}
