﻿//----------------------------------------------------------------------
// <copyright file="LogicOperator.cs" company="SerQL">
// Copyright (c) @zwei_222. All rights reserved.
// </copyright>
// <author>zwei_222</author>
// <date>2018/09/25</date>
// <summary>Logic operator class</summary>
//----------------------------------------------------------------------

using System;

namespace SerQL.Core
{
    /// <summary>
    /// Logic operator class
    /// </summary>
    public class LogicOperator : IOperator
    {
        /// <summary>
        /// The logic operators enum
        /// </summary>
        private readonly LogicOperators logicOperators;

        /// <summary>
        /// The and operator's string formats.
        /// </summary>
        private readonly string AND_QUERY_FORMAT = "{0} AND {1}";

        /// <summary>
        /// The or operator's string formats.
        /// </summary>
        private readonly string OR_QUERY_FORMAT = "{0} OR {1}";

        /// <summary>
        /// The not operator's string formats.
        /// </summary>
        private readonly string NOT_QUERY_FORMAT = "NOT {0}";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logicOperators">Logic operators enum</param>
        private LogicOperator(LogicOperators logicOperators)
        {
            this.logicOperators = logicOperators;
        }

        /// <summary>
        /// Returns the and operator. 
        /// </summary>
        /// <returns>The and operator.</returns>
        public static LogicOperator And()
        {
            return new LogicOperator(LogicOperators.AND);
        }

        /// <summary>
        /// Returns the or operator.
        /// </summary>
        /// <returns>The or operator.</returns>
        public static LogicOperator Or()
        {
            return new LogicOperator(LogicOperators.OR);
        }

        /// <summary>
        /// Returns the not operator.
        /// </summary>
        /// <returns>The not operator.</returns>
        public static LogicOperator Not()
        {
            return new LogicOperator(LogicOperators.NOT);
        }

        /// <summary>
        /// Returns the expression statements.
        /// </summary>
        /// <param name="leftHandSide">The left hand side.</param>
        /// <param name="rightHandSide">The right hand side.</param>
        /// <returns>The query string.</returns>
        public string ToQueryString(ISqlObject leftHandSide, ISqlObject rightHandSide)
        {
            var lhs = leftHandSide?.ToQueryString();
            var rhs = rightHandSide.ToQueryString();
            string format;

            switch (this.logicOperators)
            {
                case LogicOperators.AND:
                    if (lhs == null)
                    {
                        throw new ArgumentNullException(nameof(leftHandSide));
                    }

                    format = this.AND_QUERY_FORMAT;
                    break;
                case LogicOperators.OR:
                    if (lhs == null)
                    {
                        throw new ArgumentNullException(nameof(leftHandSide));
                    }

                    format = this.OR_QUERY_FORMAT;
                    break;
                case LogicOperators.NOT:
                    if (lhs != null)
                    {
                        throw new ArgumentException(nameof(leftHandSide));
                    }

                    format = this.NOT_QUERY_FORMAT;
                    break;
                default:
                    throw new ArgumentException(nameof(this.logicOperators));
            }

            if (lhs == null)
            {
                return string.Format(format, rhs);
            }
            else
            {
                return string.Format(format, lhs, rhs);
            }
        }

        /// <summary>
        /// Logic operators enum
        /// </summary>
        private enum LogicOperators
        {
            /// <summary>
            /// The and operator.
            /// </summary>
            AND,

            /// <summary>
            /// The or operator.
            /// </summary>
            OR,

            /// <summary>
            /// The not operator.
            /// </summary>
            NOT
        }
    }
}
