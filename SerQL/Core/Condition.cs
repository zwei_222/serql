﻿//----------------------------------------------------------------------
// <copyright file="Condition.cs" company="SerQL">
// Copyright (c) @zwei_222. All rights reserved.
// </copyright>
// <author>zwei_222</author>
// <date>2018/09/25</date>
// <summary>Condition class</summary>
//----------------------------------------------------------------------

namespace SerQL.Core
{
    /// <summary>
    /// Condition class
    /// </summary>
    public abstract class Condition : ISqlObject
    {
        /// <summary>
        /// Returns the conditionn statements.
        /// </summary>
        /// <returns>The query string.</returns>
        public abstract string ToQueryString();
    }
}
