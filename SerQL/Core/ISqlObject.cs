﻿//----------------------------------------------------------------------
// <copyright file="ISqlObject.cs" company="SerQL">
// Copyright (c) @zwei_222. All rights reserved.
// </copyright>
// <author>zwei_222</author>
// <date>2018/09/25</date>
// <summary>Sql objects interface</summary>
//----------------------------------------------------------------------

using System;

namespace SerQL.Core
{
    /// <summary>
    /// Sql objects interface
    /// </summary>
    public interface ISqlObject
    {
        /// <summary>
        /// Returns the query string.
        /// </summary>
        /// <returns>The query string.</returns>
        string ToQueryString();
    }
}
