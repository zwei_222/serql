﻿//----------------------------------------------------------------------
// <copyright file="IOperator.cs" company="SerQL">
// Copyright (c) @zwei_222. All rights reserved.
// </copyright>
// <author>zwei_222</author>
// <date>2018/09/25</date>
// <summary>Operator interface</summary>
//----------------------------------------------------------------------

using System;

namespace SerQL.Core
{
    /// <summary>
    /// Operator class
    /// </summary>
    public interface IOperator
    {
        /// <summary>
        /// Returns the expression statements.
        /// </summary>
        /// <param name="leftHandSide">The left hand side.</param>
        /// <param name="rightHandSide">The right hand side.</param>
        /// <returns>The query string.</returns>
        string ToQueryString(ISqlObject leftHandSide, ISqlObject rightHandSide);
    }
}
